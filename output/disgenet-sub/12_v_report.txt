Nanopublications: 14574
Head triples: 102018 (average: 7.0)
Assertion triples: 72870 (average: 5.0)
Provenance triples: 131166 (average: 9.0)
Pubinfo triples: 189462 (average: 13.0)
Total triples: 495516 (average: 34.0)
v3.0.0
Nanopublications: 24175
Head triples: 169225 (average: 7.0)
Assertion triples: 120875 (average: 5.0)
Provenance triples: 217575 (average: 9.0)
Pubinfo triples: 314275 (average: 13.0)
Total triples: 821950 (average: 34.0)
v4.0.0
Nanopublications: 45022
Head triples: 315154 (average: 7.0)
Assertion triples: 225110 (average: 5.0)
Provenance triples: 405198 (average: 9.0)
Pubinfo triples: 585286 (average: 13.0)
Total triples: 1530748 (average: 34.0)
