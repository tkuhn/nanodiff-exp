Nanodiff Study Notes
====================

WikiPathways
------------

Nanopublications:
https://jenkins.bigcat.unimaas.nl/job/WikiPathways%20Nanopublications/ws/

Data: http://data.wikipathways.org/


DisGeNET
--------

Incremental datasets:

v2.1:
- http://purl.org/np/RADYX-ia_TZYAw_eZD0-2oGGA7gnMxOnVj-Gh8wdJgAzI
- 940034 nanopubs

v3.0:
- http://purl.org/np/RAufQaKzv1pZlMhZo2eBuZtx9vuugLBJsrs4ZkvR53xzw
- 1018735 nanopubs
- 381846 reused (40.6%)

v4.0:
- http://purl.org/np/RAu0PUrg-M8HxkOiYRXkTg7r9fgOIzFZNINj8q7ywNrdM
- 1414902 nanopubs
- 860502 reused (84.5%)

Add subx prefix:

    $ less input/disgenet/v2.1.0.0.trig.gz \ 
      | sed -r 's/^@prefix this: <(.*)> .$/@prefix this: <\1> .\n@prefix subx: <\1130> ./' \
      | gzip \
      > input/disgenet/v2.1.0.0-mod.trig.gz

Temporary indexes for subsets:
- 12: http://purl.org/np/RAAGy9p2PABQk-MlGDF30myYrhJeDiqDpL9D5L82Knm3E
- 14: http://purl.org/np/RAxMyDRaM8RmKGNiEe7dQPRUTuz616iI-N2T-H3MPYmXk
- 16: http://purl.org/np/RAcf4tihZLL_aK81hwThIrNxjOhks4sEloBStEgzyR1tI


Proteinatlas
------------

Nanopublications: http://www.proteinatlas.org/about/download

Older versions: http://v15.proteinatlas.org/download/proteinatlas.trig.gz

Nanopublication counts:

- v16: 1043657 nanopubs
- v15: 1254468 nanopubs
- v14: 1254468 nanopubs
- v13: 1368080 nanopubs

Diff between Proteinatlas v14 and v15:

        30,31c30,31
        < 	dcterms:created "2015-10-16T11:00:00+02:00"^^xsd:dateTime ;
        < 	pav:versionNumber "14"^^xsd:integer . 
        ---
        > 	dcterms:created "2016-04-11T11:00:00+02:00"^^xsd:dateTime ;
        > 	pav:versionNumber "15"^^xsd:integer .
