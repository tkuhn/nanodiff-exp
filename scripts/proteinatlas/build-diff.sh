#!/bin/bash

npop reuse -n \
  -x output/$1/$2_full.trig.gz \
  -o output/$1/$3-$2_new.trig.gz \
  -c output/$1/$3_cache.txt \
  -f '--ignore-head --ignore-pubinfo' \
  input/$1/$3.trig.gz \
  &> output/$1/$3-$2_report.txt

np mkindex \
  -o output/$1/$3-$2_index.trig.gz \
  output/$1/$3_cache.txt \
  &>> output/$1/$3-$2_report.txt

