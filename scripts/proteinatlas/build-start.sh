#!/bin/bash

mkdir -p output/$1

npop reuse -n \
  -o output/$1/$2_full.trig.gz \
  -c output/$1/$2_cache.txt \
  -f '--ignore-head --ignore-pubinfo' \
  input/$1/$2.trig.gz \
  &> output/$1/$2_report.txt

np mkindex \
  -o output/$1/$2_index.trig.gz \
  output/$1/$2_cache.txt \
  &>> output/$1/$2_report.txt

