#!/bin/bash
#
# Usage: scripts/proteinatlas/preprocess.sh 16
# 

cat input/proteinatlas/original/proteinatlas-$1.trig.gz \
  | gunzip \
  | sed 's/^} ./}/' \
  | awk '{ if ($1 == "@prefix") { print $0 } else if (a == "done") { print $0 } else { if ($3 == "np:Nanopublication") { print "\n"$0; a = "done" } } }' \
  | egrep -v '^hpa:' \
  | sed -r 's/^(:.*) a np:Nanopublication .$/\1_head {\n\1 a np:Nanopublication ./' \
  | sed 's/^\(:.*\)_assertion {\s*$/}\n\1_assertion {/' \
#  | sed 's/:ENSG[0-9A-Za-z_]* prov:/prov:/' \   # this line is only for version 13
  | gzip \
  > input/proteinatlas/proteinatlas-$1.trig.gz

# alternative for version 13:  sed 's/_assertion a :IHCEvidence ;/_assertion a :IHCEvidence ./'
