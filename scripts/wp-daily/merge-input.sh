#!/bin/bash
#
# Usage: scripts/wp-daily/merge-input.sh 133
# 
# Run for many:
#
# $ for i in `seq 825 885`; do scripts/wp-daily/merge-input.sh $i; done
#

cat input/wp-daily/raw/$1/*/complexes.wp*.trig \
  | gzip \
  > input/wp-daily/complexes/$1.trig.gz

cat input/wp-daily/raw/$1/*/interactions.wp*.trig \
  | gzip \
  > input/wp-daily/interactions/$1.trig.gz

cat input/wp-daily/raw/$1/*/pathwayParticipation.wp*.trig \
  | gzip \
  > input/wp-daily/pathwayParticipation/$1.trig.gz
