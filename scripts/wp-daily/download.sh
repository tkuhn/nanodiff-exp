#!/bin/bash
#
# Download many:
#
# $ for i in `seq 830 885`; do scripts/wp-daily/download.sh $i; done
#

if [[ $1 == "latest" ]]; then

  wget -O input/wp-daily/raw/latest.zip \
    'https://jenkins.bigcat.unimaas.nl/job/WikiPathways%20Nanopublications/ws/*zip*/workspace.zip'

else

  wget -O input/wp-daily/raw/$1-h.zip \
    "https://jenkins.bigcat.unimaas.nl/job/WikiPathways%20Nanopublications/$1/artifact/*zip*/archive.zip"

  unzip -o -q -d input/wp-daily/raw/$1 input/wp-daily/raw/$1-h.zip

fi
