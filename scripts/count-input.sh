#!/bin/bash
#
# Usage:
# $ scripts/count-input.sh wp-monthly/complexes HEADER
# $ scripts/count-input.sh wp-monthly/complexes 20161210

if [ $2 == "HEADER" ]; then
  echo "filename,nanopubs,head,assertion,provenance,pubinfo" \
    >  output/$1/0_input_count.csv
  exit
fi

npop count \
  -r output/$1/0_input_count.csv \
  input/$1/$2.trig.gz
