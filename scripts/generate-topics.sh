#!/bin/bash
#
# Usage:
#
# $ export IGNORE='...'
# $ scripts/generate-topics.sh wp-daily 133
#
# $ scripts/generate-topics.sh wp-monthly pathwayParticipation/20161210 '-d WikipathwaysTopics'
#
# For WikiPathways:
#
# export IGNORE='http://www.w3.org/1999/02/22-rdf-syntax-ns#type|http://vocabularies.wikipathways.org/wp#pathwayOntologyTag'
#

npop topic -u input/$1/$2.trig.gz -i "$IGNORE" $3 \
  > output/$1/$2_topics.txt

(
  echo "Nanopubs without topic:" ;
  cat output/$1/$2_topics.txt | grep '^null ' | wc -l ;
  echo "Nanopubs with topic:" ;
  cat output/$1/$2_topics.txt | grep -v '^null ' | wc -l ;
  echo "Unique topics:" ;
  cat output/$1/$2_topics.txt | grep -v '^null ' | awk '{ print $1 }' | sort -u | wc -l ;
) > output/$1/$2_topics_report.txt
