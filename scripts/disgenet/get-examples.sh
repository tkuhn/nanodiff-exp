#!/bin/bash

echo "version 2.1"

npop filter \
  -f http://identifiers.org/ncbigene/4885 \
  input/disgenet/v2.1.0.0.trig.gz \
  > scratch/v2_4885.trig

np check scratch/v2_4885.trig

npop filter \
  -f http://linkedlifedata.com/resource/umls/id/C0151786 \
  scratch/v2_4885.trig \
  > scratch/v2_4885_C0151786.trig

np check scratch/v2_4885_C0151786.trig

npop fingerprint -h DisgenetFingerprints scratch/v2_4885_C0151786.trig

echo "version 3.0"

npop filter \
  -f http://identifiers.org/ncbigene/4885 \
  input/disgenet/v3.0.0.0.trig.gz \
  > scratch/v3_4885.trig

np check scratch/v3_4885.trig

npop filter \
  -f http://linkedlifedata.com/resource/umls/id/C0151786 \
  scratch/v3_4885.trig \
  > scratch/v3_4885_C0151786.trig

np check scratch/v3_4885_C0151786.trig

npop fingerprint -h DisgenetFingerprints scratch/v3_4885_C0151786.trig

echo "version 4.0"

npop filter \
  -f http://identifiers.org/ncbigene/4885 \
  input/disgenet/v4.0.0.0.trig.gz \
  > scratch/v4_4885.trig

np check scratch/v4_4885.trig

npop filter \
  -f http://linkedlifedata.com/resource/umls/id/C0151786 \
  scratch/v4_4885.trig \
  > scratch/v4_4885_C0151786.trig

np check scratch/v4_4885_C0151786.trig

npop fingerprint -h DisgenetFingerprints scratch/v4_4885_C0151786.trig

