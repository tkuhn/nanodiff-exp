#!/bin/bash
#
# Usage:
# $ scripts/disgenet/extract-subset.sh 1 1_identifiers.txt.gz
#

npop filter \
  -F literature/$2 \
  -o output/disgenet-sub/$1.trig.gz \
  output/disgenet/v4.0.0.0_full.trig.gz

npop count \
  output/disgenet-sub/$1.trig.gz \
  > output/disgenet-sub/$1_report.txt
