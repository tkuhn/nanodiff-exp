#!/bin/bash
#
# Usage:
# $ scripts/disgenet/test-subset-download.sh 14 http://purl.org/np/RAxMyDRaM8RmKGNiEe7dQPRUTuz616iI-N2T-H3MPYmXk
#

rm -f output/disgenet-sub/download-test-${1}.txt

for i in `seq 1 10`; do
  echo "Download $i" >> output/disgenet-sub/download-test-${1}.txt
  ( time np get -c -o scratch/${1}_${i}.trig.gz $2 ) \
    >> output/disgenet-sub/download-test-${1}.txt 2>&1
  sleep 20
done

