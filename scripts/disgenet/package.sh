#!/bin/bash
#
# Usage:
# $ scripts/disgenet/package.sh
#

(
  ( cat output/disgenet/v2.1.0.0_index.trig.gz | gunzip ) ;
  ( cat output/disgenet/v2.1.0.0_full.trig.gz | gunzip )
) \
  | gzip \
  > package/disgenet/disgenet_v2.1.0.0_incremental.trig.gz

(
  ( cat output/disgenet/v3.0.0.0-v2.1.0.0_index.trig.gz | gunzip ) ;
  ( cat output/disgenet/v3.0.0.0-v2.1.0.0_new.trig.gz | gunzip )
) \
  | gzip \
  > package/disgenet/disgenet_v3.0.0.0_incremental.trig.gz

(
  ( cat output/disgenet/v4.0.0.0-v3.0.0.0_index.trig.gz | gunzip ) ;
  ( cat output/disgenet/v4.0.0.0-v3.0.0.0_new.trig.gz | gunzip )
) \
  | gzip \
  > package/disgenet/disgenet_v4.0.0.0_incremental.trig.gz
