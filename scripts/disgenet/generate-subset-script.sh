#!/bin/bash

cat literature/disgenet-subsets.txt \
  | sed 's|umls:|http://linkedlifedata.com/resource/umls/id/|g' \
  | sed 's|ncbigene:|http://identifiers.org/ncbigene/|g' \
  | sed -r 's|^([0-9]+) (.*)$|scripts/disgenet/extract-subset.sh \1 "\2"|' \
  | sed -r 's|.sh ([0-9]+) "(.+).txt.gz"|-f.sh \1 \2.txt.gz|g' \
  > scripts/disgenet/extract-all-subsets.sh
