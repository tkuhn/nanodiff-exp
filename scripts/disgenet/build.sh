#!/bin/bash

scripts/disgenet/build-start.sh v2.1.0.0
scripts/disgenet/build-diff.sh v2.1.0.0 v3.0.0.0
scripts/disgenet/build-diff.sh v3.0.0.0 v4.0.0.0
