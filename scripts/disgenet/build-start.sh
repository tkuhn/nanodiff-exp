#!/bin/bash
#
# Usage: scripts/disgenet/build-start.sh v2.1.0.0
#

mkdir -p output/disgenet

echo "file,reusableCount,inputCount,reuseCount,topicMatchCount,intopicDuplCount,outTopicDuplCount,topicMatchErrors" \
  >> output/disgenet/0_ctable.csv
echo "file,reuseCount" \
  >> output/disgenet/0_itable.csv

npop reuse -n \
  -o output/disgenet/$1_full.trig.gz \
  -c output/disgenet/$1_cache.txt \
  -r output/disgenet/0_ctable.csv \
  -f '-h DisgenetFingerprints' \
  -t '-h DisgenetTopics' \
  input/disgenet/$1.trig.gz \
  &> output/disgenet/$1_report.txt

npop ireuse -s \
  -o output/disgenet/$1_index.trig.gz \
  -r output/disgenet/0_itable.csv \
  -T "Nanopubs extracted from DisGeNET $1, incremental dataset" \
  -D "These nanopubs were automatically extracted from the DisGeNET dataset." \
  -C "0000-0003-0169-8159" \
  -C "0000-0002-9383-528X" \
  -C "0000-0002-1267-0234" \
  output/disgenet/$1_cache.txt \
  &>> output/disgenet/$1_report.txt

