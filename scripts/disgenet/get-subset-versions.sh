#!/bin/bash
#
# Usage:
# $ scripts/disgenet/calculate-subset-versions.sh 6
#

echo "v2.1.0" > output/disgenet-sub/$1_v_report.txt

npop filter \
  -f "v2.1.0" \
  -o output/disgenet-sub/$1_v2.trig.gz \
  output/disgenet-sub/$1.trig.gz

npop count \
  output/disgenet-sub/$1_v2.trig.gz \
  > output/disgenet-sub/$1_v_report.txt

rm output/disgenet-sub/$1_v2.trig.gz


echo "v3.0.0" >> output/disgenet-sub/$1_v_report.txt

npop filter \
  -f "v3.0.0" \
  -o output/disgenet-sub/$1_v3.trig.gz \
  output/disgenet-sub/$1.trig.gz

npop count \
  output/disgenet-sub/$1_v3.trig.gz \
  >> output/disgenet-sub/$1_v_report.txt

rm output/disgenet-sub/$1_v3.trig.gz


echo "v4.0.0" >> output/disgenet-sub/$1_v_report.txt

npop filter \
  -f "v4.0.0" \
  -o output/disgenet-sub/$1_v4.trig.gz \
  output/disgenet-sub/$1.trig.gz

npop count \
  output/disgenet-sub/$1_v4.trig.gz \
  >> output/disgenet-sub/$1_v_report.txt

rm output/disgenet-sub/$1_v4.trig.gz
