#!/bin/bash
#
# Usage:
# $ scripts/disgenet/extract-subset.sh 6 \
#    "http://linkedlifedata.com/resource/umls/id/C0152021 http://linkedlifedata.com/resource/umls/id/C0018798"
#

npop filter --split \
  -f "$2" \
  -o output/disgenet-sub/$1.trig.gz \
  output/disgenet/v4.0.0.0_full.trig.gz

npop count \
  output/disgenet-sub/$1.trig.gz \
  > output/disgenet-sub/$1_report.txt
