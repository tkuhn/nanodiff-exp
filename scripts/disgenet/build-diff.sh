#!/bin/bash
#
# Usage: scripts/disgenet/build-diff.sh v2.1.0.0 v3.0.0.0
#
# Publish indexes to get *_full files:
# $ np publish output/disgenet/v3.0.0.0_index.trig.gz
# $ np get -c -o output/disgenet/v3.0.0.0_full.trig.gz http://purl.org/np/RA...
#

npop reuse -n \
  -x output/disgenet/$1_cache.txt \
  -o output/disgenet/$2-$1_new.trig.gz \
  -c output/disgenet/$2_cache.txt \
  -r output/disgenet/0_ctable.csv \
  -f '-h DisgenetFingerprints' \
  -t '-h DisgenetTopics' \
  input/disgenet/$2.trig.gz \
  &> output/disgenet/$2-$1_report.txt

npop ireuse -s \
  -x output/disgenet/$1_index.trig.gz \
  -o output/disgenet/$2-$1_index.trig.gz \
  -a output/disgenet/$2_index.trig.gz \
  -r output/disgenet/0_itable.csv \
  -T "Nanopubs extracted from DisGeNET $2, incremental dataset" \
  -D "These nanopubs were automatically extracted from the DisGeNET dataset." \
  -C "0000-0003-0169-8159" \
  -C "0000-0002-9383-528X" \
  -C "0000-0002-1267-0234" \
  output/disgenet/$2_cache.txt \
  &>> output/disgenet/$2-$1_report.txt

