#!/bin/bash
#
# Usage:
# $ scripts/disgenet/test-full-download.sh
#

rm -f output/disgenet/download-test.txt

for i in `seq 1 10`; do
  echo "Download $i" >> output/disgenet/download-test-${1}.txt
  ( time wget -O scratch/v4.0.0.0-test-${i}.trig.gz http://rdf.disgenet.org/download/v4.0.0/nanopublications_v4.0.0.0.trig.gz ) \
    >> output/disgenet/download-test.txt 2>&1
  sleep 20
done

