#!/bin/bash
#
# Usage:
#
# $ scripts/decontextualize.sh disgenet/v2.1.0.0
# 

echo "Decontextualize..."
npop decontext -o output/$1_norm.nq.gz input/$1.trig.gz
echo "Number of decontextualized triples:"
cat output/$1_norm.nq.gz | gunzip | wc -l
echo "Sorting..."
cat output/$1_norm.nq.gz | gunzip | sort -u | gzip > output/$1_norm_uniq.nq.gz
echo "Number of unique decontextualized triples:"
cat output/$1_norm_uniq.nq.gz | gunzip | wc -l
