#!/bin/bash
#
# Usage:
#
# $ scripts/build-diff.sh wp-daily/complexes 825 826
# $ scripts/build-diff.sh wp-monthly/complexes 20161210 20170210
#
# Run for many:
#
# $ for i in `seq 825 884`; do scripts/build-diff.sh wp-daily/complexes $i $((i+1)); done
#

npop reuse -n \
  -x output/$1/$2_full.trig.gz \
  -o output/$1/$3-$2_new.trig.gz \
  -a output/$1/$3_full.trig.gz \
  -c output/$1/$3_cache.txt \
  -r output/$1/0_ctable.csv \
  input/$1/$3.trig.gz \
  &> output/$1/$3-$2_report.txt

npop ireuse \
  -x output/$1/$2_index.trig.gz \
  -o output/$1/$3-$2_index.trig.gz \
  -a output/$1/$3_index.trig.gz \
  -r output/$1/0_itable.csv \
  output/$1/$3_cache.txt \
  &>> output/$1/$3-$2_report.txt

