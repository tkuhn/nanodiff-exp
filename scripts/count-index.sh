#!/bin/bash
#
# Usage:
# $ scripts/count-index.sh wp-monthly/complexes HEADER
# $ scripts/count-index.sh wp-monthly/complexes 20161210

if [ $2 == "HEADER" ]; then
  echo "filename,nanopubs,head,assertion,provenance,pubinfo" \
    > output/$1/0_icount.csv
  exit
fi

if [ -f output/$1/$2_index.trig.gz ]; then
  npop count \
    -r output/$1/0_icount.csv \
    output/$1/$2_index.trig.gz
fi
if [ -f output/$1/$2-*_index.trig.gz ]; then
  npop count \
    -r output/$1/0_icount.csv \
    output/$1/$2-*_index.trig.gz
fi
