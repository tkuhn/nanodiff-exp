#!/bin/bash
#
# Usage:
#
# $ scripts/build-start.sh wp-daily/complexes 825
# $ scripts/build-start.sh wp-monthly/complexes 20161210
#

mkdir -p output/$1

echo "file,reusableCount,inputCount,reuseCount,topicMatchCount,intopicDuplCount,outTopicDuplCount,topicMatchErrors" \
  >> output/$1/0_ctable.csv
echo "file,reuseCount" \
  >> output/$1/0_itable.csv

npop reuse -n \
  -o output/$1/$2_full.trig.gz \
  -c output/$1/$2_cache.txt \
  -r output/$1/0_ctable.csv \
  input/$1/$2.trig.gz \
  &> output/$1/$2_report.txt

npop ireuse \
  -o output/$1/$2_index.trig.gz \
  -r output/$1/0_itable.csv \
  output/$1/$2_cache.txt \
  &>> output/$1/$2_report.txt
