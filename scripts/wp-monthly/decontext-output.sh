#!/bin/bash
#
# Usage:
# $ scripts/wp-monthly/decontext-output.sh 20161210
#

npop decontext \
  -o output/wp-monthly/combined/$1-decontext.nq.gz \
  output/wp-monthly/combined/$1_full.trig.gz

cat output/wp-monthly/combined/$1-decontext.nq.gz \
  | gunzip \
  | sort -u \
  | gzip \
  > output/wp-monthly/combined/$1-decontext-uniq.nq.gz

cat output/wp-monthly/combined/$1-decontext-uniq.nq.gz \
  | gunzip \
  | wc -l
