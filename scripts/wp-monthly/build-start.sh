#!/bin/bash
#
# Usage:
#
# $ scripts/wp-monthly/build-start.sh complexes 20161210
#

mkdir -p output/$1

echo "file,reusableCount,inputCount,reuseCount,topicMatchCount,intopicDuplCount,outTopicDuplCount,topicMatchErrors" \
  >> output/wp-monthly/$1/0_ctable.csv
echo "file,reuseCount" \
  >> output/wp-monthly/$1/0_itable.csv

npop reuse -n -s \
  -o output/wp-monthly/$1/$2_full.trig.gz \
  -c output/wp-monthly/$1/$2_cache.txt \
  -r output/wp-monthly/$1/0_ctable.csv \
  -f '-h WikipathwaysFingerprints' \
  -t '-h WikipathwaysTopics' \
  input/wp-monthly/$1/$2.trig.gz \
  &> output/wp-monthly/$1/$2_report.txt

npop ireuse -s \
  -o output/wp-monthly/$1/$2_index.trig.gz \
  -r output/wp-monthly/$1/0_itable.csv \
  -T "Nanopubs covering $1 extracted from WikiPathways version $2" \
  -C "0000-0001-7542-0286" \
  -C "0000-0002-1267-0234" \
  output/wp-monthly/$1/$2_cache.txt \
  &>> output/wp-monthly/$1/$2_report.txt
