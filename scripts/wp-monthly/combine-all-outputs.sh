#!/bin/bash

releases=(
  "20160610" "20160710" "20160810" "20160910" "20161010" "20161110"
  "20161210" "20170210" "20170310" "20170410" "20170510"
)

for i in "${releases[@]}"; do
  scripts/wp-monthly/combine-output.sh $i
done
