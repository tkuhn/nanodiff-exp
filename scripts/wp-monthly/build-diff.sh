#!/bin/bash
#
# Usage:
#
# $ scripts/wp-monthly/build-diff.sh complexes 20161210 20170210
#

npop reuse -n -s \
  -x output/wp-monthly/$1/$2_full.trig.gz \
  -o output/wp-monthly/$1/$3-$2_new.trig.gz \
  -a output/wp-monthly/$1/$3_full.trig.gz \
  -c output/wp-monthly/$1/$3_cache.txt \
  -r output/wp-monthly/$1/0_ctable.csv \
  -t '-h WikipathwaysTopics' \
  -f '-h WikipathwaysFingerprints' \
  input/wp-monthly/$1/$3.trig.gz \
  &> output/wp-monthly/$1/$3-$2_report.txt

npop ireuse -s \
  -x output/wp-monthly/$1/$2_index.trig.gz \
  -o output/wp-monthly/$1/$3-$2_index.trig.gz \
  -a output/wp-monthly/$1/$3_index.trig.gz \
  -r output/wp-monthly/$1/0_itable.csv \
  -T "Nanopubs covering $1 extracted from WikiPathways version $3" \
  -C "0000-0001-7542-0286" \
  -C "0000-0002-1267-0234" \
  output/wp-monthly/$1/$3_cache.txt \
  &>> output/wp-monthly/$1/$3-$2_report.txt

