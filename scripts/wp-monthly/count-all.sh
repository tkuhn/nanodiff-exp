#!/bin/bash

releases=(
  "20160610" "20160710" "20160810" "20160910" "20161010" "20161110"
  "20161210" "20170210" "20170310" "20170410" "20170510"
)

scripts/count.sh wp-monthly/complexes HEADER
scripts/count-index.sh wp-monthly/complexes HEADER
scripts/count.sh wp-monthly/interactions HEADER
scripts/count-index.sh wp-monthly/interactions HEADER
scripts/count.sh wp-monthly/pathwayParticipation HEADER
scripts/count-index.sh wp-monthly/pathwayParticipation HEADER
scripts/count.sh wp-monthly/combined HEADER
scripts/count-index.sh wp-monthly/combined HEADER

for i in "${releases[@]}"; do
  scripts/count.sh wp-monthly/complexes $i
  scripts/count-index.sh wp-monthly/complexes $i
  scripts/count.sh wp-monthly/interactions $i
  scripts/count-index.sh wp-monthly/interactions $i
  scripts/count.sh wp-monthly/pathwayParticipation $i
  scripts/count-index.sh wp-monthly/pathwayParticipation $i
  scripts/count.sh wp-monthly/combined $i
  scripts/count-index.sh wp-monthly/combined $i
done
