#!/bin/bash
#
# Usage:
# $ scripts/wp-monthly/combine-output.sh 20161210
#

# Full

mkdir -p output/wp-monthly/combined

cat output/wp-monthly/complexes/$1_full.trig.gz \
  | gunzip \
  > output/wp-monthly/combined/$1_full.trig

cat output/wp-monthly/interactions/$1_full.trig.gz \
  | gunzip \
  >> output/wp-monthly/combined/$1_full.trig

if [ -f output/wp-monthly/pathwayParticipation/$1_full.trig.gz ]; then
  cat output/wp-monthly/pathwayParticipation/$1_full.trig.gz \
    | gunzip \
    >> output/wp-monthly/combined/$1_full.trig
fi

cat output/wp-monthly/combined/$1_full.trig \
  | gzip \
  > output/wp-monthly/combined/$1_full.trig.gz

rm output/wp-monthly/combined/$1_full.trig


# New

mkdir -p output/wp-monthly/combined

touch output/wp-monthly/combined/$1-xx_new.trig

if [ -f output/wp-monthly/complexes/$1-*_new.trig.gz ]; then
  cat output/wp-monthly/complexes/$1-*_new.trig.gz \
    | gunzip \
    > output/wp-monthly/combined/$1-xx_new.trig
fi

if [ -f output/wp-monthly/interactions/$1-*_new.trig.gz ]; then
  cat output/wp-monthly/interactions/$1-*_new.trig.gz \
    | gunzip \
    >> output/wp-monthly/combined/$1-xx_new.trig
fi

if [ -f output/wp-monthly/pathwayParticipation/$1-*_new.trig.gz ]; then
  cat output/wp-monthly/pathwayParticipation/$1-*_new.trig.gz \
    | gunzip \
    >> output/wp-monthly/combined/$1-xx_new.trig
fi

cat output/wp-monthly/combined/$1-xx_new.trig \
  | gzip \
  > output/wp-monthly/combined/$1-xx_new.trig.gz

rm output/wp-monthly/combined/$1-xx_new.trig


# Full Index

mkdir -p output/wp-monthly/combined

cat output/wp-monthly/complexes/$1_index.trig.gz \
  | gunzip \
  > output/wp-monthly/combined/$1_index.trig

cat output/wp-monthly/interactions/$1_index.trig.gz \
  | gunzip \
  >> output/wp-monthly/combined/$1_index.trig

if [ -f output/wp-monthly/pathwayParticipation/$1_index.trig.gz ]; then
  cat output/wp-monthly/pathwayParticipation/$1_index.trig.gz \
    | gunzip \
    >> output/wp-monthly/combined/$1_index.trig
fi

cat output/wp-monthly/top/$1_top-index.trig.gz \
    | gunzip \
    >> output/wp-monthly/combined/$1_index.trig

cat output/wp-monthly/combined/$1_index.trig \
  | gzip \
  > output/wp-monthly/combined/$1_index.trig.gz

rm output/wp-monthly/combined/$1_index.trig


# New Index

mkdir -p output/wp-monthly/combined

touch output/wp-monthly/combined/$1-xx_index.trig

if [ -f output/wp-monthly/complexes/$1-*_index.trig.gz ]; then
  cat output/wp-monthly/complexes/$1-*_index.trig.gz \
    | gunzip \
    > output/wp-monthly/combined/$1-xx_index.trig
fi

if [ -f output/wp-monthly/interactions/$1-*_index.trig.gz ]; then
  cat output/wp-monthly/interactions/$1-*_index.trig.gz \
    | gunzip \
    >> output/wp-monthly/combined/$1-xx_index.trig
fi

if [ -f output/wp-monthly/pathwayParticipation/$1-*_index.trig.gz ]; then
  cat output/wp-monthly/pathwayParticipation/$1-*_index.trig.gz \
    | gunzip \
    >> output/wp-monthly/combined/$1-xx_index.trig
fi

cat output/wp-monthly/top/$1_top-index.trig.gz \
    | gunzip \
    >> output/wp-monthly/combined/$1-xx_index.trig

cat output/wp-monthly/combined/$1-xx_index.trig \
  | gzip \
  > output/wp-monthly/combined/$1-xx_index.trig.gz

rm output/wp-monthly/combined/$1-xx_index.trig
