#!/bin/bash

releases=(
  "20160610" "20160710" "20160810" "20160910" "20161010" "20161110"
  "20161210" "20170210" "20170310" "20170410" "20170510"
)

mkdir -p output/wp-monthly/top
rm -f output/wp-monthly/top/top-indexes_report.txt

for i in "${releases[@]}"; do
  echo "---"
  INDEX_COMPLEXES=`
    cat output/wp-monthly/complexes/$i*_report.txt \
    | egrep '^Index URI: ' | tail -1 | sed 's/Index URI: //'
  `
  INDEX_INTERACTIONS=`
    cat output/wp-monthly/interactions/$i*_report.txt \
    | egrep '^Index URI: ' | tail -1 | sed 's/Index URI: //'
  `
  if [ -f output/wp-monthly/pathwayParticipation/${i}*_report.txt ]; then
    INDEX_PWPARTICIPATION=`
      cat output/wp-monthly/pathwayParticipation/$i*_report.txt \
      | egrep '^Index URI: ' | tail -1 | sed 's/Index URI: //'
    `
  fi
  SUB="-s $INDEX_COMPLEXES -s $INDEX_INTERACTIONS"
  if [ $INDEX_PWPARTICIPATION ]; then
    SUB="$SUB -s $INDEX_PWPARTICIPATION"
  fi
  SUPERSEDE=""
  if [ $PREVIOUS ]; then
    SUPERSEDE="-x $PREVIOUS"
  fi
  (
    echo "$i";
    np mkindex \
      -t "Nanopublications extracted from WikiPathways, incremental dataset, $i" \
      -c "0000-0001-7542-0286" \
      -c "0000-0002-1267-0234" \
      $SUB \
      $SUPERSEDE \
      -o output/wp-monthly/top/${i}_top-index.trig.gz
  ) >> output/wp-monthly/top/top-indexes_report.txt
  PREVIOUS=`
    cat output/wp-monthly/top/top-indexes_report.txt \
    | egrep '^Index URI: ' | tail -1 | sed 's/Index URI: //'
  `
done
