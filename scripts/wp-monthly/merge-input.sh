#!/bin/bash
#
# Usage: scripts/wp-monthly/merge-input.sh complexes 20161210
# 

cat input/wp-monthly/raw/$2/$1.wp*.trig \
  | gzip \
  > input/wp-monthly/$1/$2.trig.gz

