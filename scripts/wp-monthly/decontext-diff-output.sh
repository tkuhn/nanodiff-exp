#!/bin/bash
#
# Usage:
# $ scripts/wp-monthly/decontext-diff-output.sh 20161210 20170210
#

sort \
  <( cat output/wp-monthly/combined/$1-decontext-uniq.nq.gz \
     | gunzip ) \
  <( cat output/wp-monthly/combined/$2-decontext-uniq.nq.gz \
     | gunzip ) \
  | uniq -u \
  | gzip \
  > output/wp-monthly/combined/$2-$1-decontext-changed.nq.gz

cat output/wp-monthly/combined/$2-$1-decontext-changed.nq.gz \
  | gunzip \
  | wc -l
