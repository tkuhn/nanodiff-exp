#!/bin/bash

mkdir -p output/wp-monthly/combined/
mkdir -p output/wp-monthly/complexes/
mkdir -p output/wp-monthly/interactions/
mkdir -p output/wp-monthly/pathwayParticipation/

releases=(
  "20160610" "20160710" "20160810" "20160910" "20161010" "20161110"
  "20161210" "20170210" "20170310" "20170410" "20170510"
)

echo "Build wp-monthly/complexes..."
scripts/wp-monthly/build-start.sh complexes "${releases[0]}"
for i in `seq 0 9`; do
  scripts/wp-monthly/build-diff.sh complexes "${releases[$i]}" "${releases[$i+1]}"
done

echo "Build wp-monthly/interactions..."
scripts/wp-monthly/build-start.sh interactions "${releases[0]}"
for i in `seq 0 9`; do
  scripts/wp-monthly/build-diff.sh interactions "${releases[$i]}" "${releases[$i+1]}"
done

# The first five releases don't contain any pathwayParticipation nanopubs:
releases=(
  "20161110" "20161210" "20170210" "20170310" "20170410" "20170510"
)

echo "Build wp-monthly/pathwayParticipation..."
scripts/wp-monthly/build-start.sh pathwayParticipation "${releases[0]}"
for i in `seq 0 4`; do
  scripts/wp-monthly/build-diff.sh pathwayParticipation "${releases[$i]}" "${releases[$i+1]}"
done
