#!/bin/bash

releases=(
  "20160610" "20160710" "20160810" "20160910" "20161010" "20161110"
  "20161210" "20170210" "20170310" "20170410" "20170510"
)

scripts/count-input.sh wp-monthly/complexes HEADER
scripts/count-input.sh wp-monthly/interactions HEADER
scripts/count-input.sh wp-monthly/pathwayParticipation HEADER

for i in "${releases[@]}"; do
  scripts/count-input.sh wp-monthly/complexes $i
  scripts/count-input.sh wp-monthly/interactions $i
  scripts/count-input.sh wp-monthly/pathwayParticipation $i
done
