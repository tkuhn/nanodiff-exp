Nanodiff Study
==============

This repository contains the code for the studies conducted for this paper:

- Tobias Kuhn, Egon Willighagen, Chris Evelo, Núria Queralt-Rosinach, Emilio
  Centeno, and Laura I. Furlong. Reliable Granular References to Changing Linked
  Data. In Proceedings of the 16th International Semantic Web Conference (ISWC).
  Springer, 2017.

The resulting datasets can be accessed via the server network:

- [WikiPathways](http://purl.org/np/RAKz0OQ3Dq8dDWqF7SIY4TgYcZRX4d2TnmLUEbOwnaGmQ)
- [DisGeNET v2.1.0.0](http://purl.org/np/RADYX-ia_TZYAw_eZD0-2oGGA7gnMxOnVj-Gh8wdJgAzI)
- [DisGeNET v3.0.0.0](http://purl.org/np/RAufQaKzv1pZlMhZo2eBuZtx9vuugLBJsrs4ZkvR53xzw)
- [DisGeNET v4.0.0.0](http://purl.org/np/RAu0PUrg-M8HxkOiYRXkTg7r9fgOIzFZNINj8q7ywNrdM)
